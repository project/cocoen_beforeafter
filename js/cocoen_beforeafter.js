(function($, Drupal, once) {

  Drupal.behaviors.beforeafter = {
    attach: function(context) {
      once('cocoen', '.cocoen-beforeafter-container', context).forEach(function (element) {
          $(element).cocoen();
      });
    }
  }

})(jQuery, Drupal, once);
