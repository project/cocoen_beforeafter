## Cocoen BeforeAfter

This module provides a new formatter for image and
media image fields with beforeAfter effect to compare images.

## INSTALLATION

- Download and enable module.
- Download plugin and extract to /libraries directory,
  correct path should be /libraries/cocoen/dist/js/cocoen.min.js.
- Select cocoen beforeAfter formater,
  remember that you have to upload 2 images.
