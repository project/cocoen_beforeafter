<?php

namespace Drupal\cocoen_beforeafter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\image\Entity\ImageStyle;

/**
 * Plugin implementation of the 'Cocoen Before After Media' formatter.
 *
 * @FieldFormatter(
 *   id = "cocoen_before_after_media",
 *   label = @Translation("Cocoen Before After Media"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class CocoenBeforeAfterMediaFormatter extends CocoenBeforeAfterImageFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    if (empty($items)) {
      return [];
    }

    $settings = $this->getSettings();
    $style = ImageStyle::load($settings['image_style']);
    $images = [];

    foreach ($items as $delta => $item) {
      $media = $item->entity;
      $source_field_name = $media->getSource()->getConfiguration()['source_field'];
      $imageEntity = $media->get($source_field_name)->entity;

      if ($imageEntity !== NULL) {
        $images[] = isset($style) ? $style->buildUrl($imageEntity->getFileUri()) :
          $this->fileUrlGenerator->generateAbsoluteString($imageEntity->getFileUri());
      }

      if (count($images) == 2) {
        break;
      }
    }

    return [
      '#theme' => 'cocoen_before_after_image',
      '#images' => $images,
      '#attached' => [
        'library' => [
          'cocoen_beforeafter/cocoen_beforeafter',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // This formatter is only available for entity types that reference
    // media items.
    return ($field_definition->getFieldStorageDefinition()->getSetting('target_type') == 'media');
  }

}
